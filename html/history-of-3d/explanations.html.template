<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>3DCG: History of 3D Graphics</title>
    <%= stylesheets('3dcg', 'box2') %>
    <%= scripts('jquery', 'underscore') %>
    <style>
      img.large {
        width: 60%;
      }

      table.centered {
        margin-left: auto;
        margin-right: auto;
      }

      table.tabular {
        border-collapse: collapse;
      }

      table.tabular tr:first-child {
        background: #AAA;
      }

      table.tabular th {
        text-align: center;
        padding-left: 1em;
        padding-right: 1em;
      }

      table.tabular td {
        text-align: center;
      }

      table.game {
        border: 1px solid black;
        border-collapse: collapse;
        margin: 5px auto;
      }
      
      table.game td:first-child {
        background: #AAA;
        width: 10em;
        text-align: right;
        font-weight: bold;
        padding: 5px;
      }

      table.game td {
        text-align: center;
        width: 20em;
      }

      div.video {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
      }
    </style>

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
      
      function initialize()
      {
      }

      $( initialize );    
    </script>
    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML"></script>
  </head>

  <body>
    <header>
      Ray Tracing
    </header>
    <div id="contents">
      <section>
        <h1>History of 3D Computer Graphics</h1>
        <p>
          Computer Graphics is a relatively young field and, as is typical
          of things that are still in their early developmental stages,
          many different approaches are explored. This was very much the case
          with computer games during the 1990s.
        </p>
        <p>
          Below are a series of games, each with their unique approach to computer graphics.
        </p>
        <section>
          <h2><a href="http://playdosgamesonline.com/wolfenstein-3d.html">Wolfenstein 3D</a></h2>
          <%= youtube 'NdcnQISuF_Y' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Wolfenstein 3D</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1992</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td><a href="https://en.wikipedia.org/wiki/Intel_80286">80286</a></td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>528KB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>Ray casting + sprites</td>
              </tr>
            </tbody>
          </table>
          <p>
            The world was represented internally as a 2D grid, meaning walls could only be placed at 90&deg; angles; e.g. a wall making a 45&deg; angle was not supported by the game engine.
            Floors and ceilings had fixed heights: there were no stairs, no huge halls, no holes in the ground. The world was truly flat.
            This engine could only be used to render indoor game worlds convincingly.
          </p>
          <%= tex_image('wolfenstein-world', style:"width: 40%;") %>
          <p>
            Enemies were represented using sprites, i.e. flat bitmaps that were drawn where the enemy was. The closer the enemy was,
            the more stretched out the bitmap had to be, leading to serious pixellation when you got up close to an enemy.
          </p>
        </section>
        
        <section>
          <h2><a href="http://playdosgamesonline.com/Alone-in-the-dark.html">Alone in the Dark</a></h2>
          <%= youtube 'iSwYY2eoKhQ' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Alone in the Dark</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1992</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td><a href="https://en.wikipedia.org/wiki/Intel_80386">80386</a></td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>4MB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>2D Backgrounds + rasterization</td>
              </tr>
            </tbody>
          </table>
          <p>
            The game world itself was rendered in a fully 2D fashion (backgrounds were prerendered bitmaps), but the creatures roaming about were three-dimensional.
            A creature is made out of a bunch of triangles. When a creature needs to be rendered, the triangles are first transformed in 3D space (depending
            on the creature's orientation in the game world and the camera position) after which they are projected onto the screen. This process is called <em>rasterization</em>.
            These triangles were flat shaded, meaning that each triangle was drawn using exactly one color (i.e. no textures or lighting.)
          </p>
        </section>

        <section>
          <h2><a href="https://en.wikipedia.org/wiki/Comanche_(series)">Comanche</a></h2>
          <%= youtube 'snWmPWfeS6Y' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Comanche</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1992</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td><a href="https://en.wikipedia.org/wiki/Intel_80386">80386</a></td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>4MB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>2D Backgrounds + flated shaded triangles</td>
              </tr>
            </tbody>
          </table>
          <p>
            Comanche sported a fully 3D world which it rendered using voxels (volumetric pixels). This is similar to MineCraft, but with much smaller 'boxes'.
            This approach allowed it to produce convincing outside worlds with great detail.
          </p>
        </section>

        <section>
          <h2><a href="http://playdosgamesonline.com/doom-evil-unleashed.html">Doom</a></h2>
          <%= youtube '8mEP4cflrd4' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Doom</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1993</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td><a href="https://en.wikipedia.org/wiki/Intel_80386">80386</a></td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>4MB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>Ray casting + sprites</td>
              </tr>
            </tbody>
          </table>
          <p>
            Doom required a <a href="https://en.wikipedia.org/wiki/Intel_80386">80386</a> with 4MB of memory.
            The game world was internally represented by 2D polygons, meaning this game was still not truly 3D. Each 2D-polygon was allowed to
            have its own floor and ceiling height. For example, using many small such polygons allowed you to model stairs.
          </p>
          <%= tex_image('doom-world', style:"width: 40%;") %>
          <p>
            The world was still "flat" in a way: these 2D polygons could not overlap, so you could not have a bridge for which
            it was possible both to walk on and under. You could not have a building with several floors on top of each other.
            (Actually, that's a bit of a lie: floors that were supposedly on above the other were internally modelled
            as areas placed adjacent next to each other. An elevator that pretended moving you up and down would actually teleport you
            horizontally.) Like Wolfenstein 3D, this approach is mostly limited to render indoor gaming worlds.
          </p>
          <p>
            Enemies were represented using sprites.
          </p>
        </section>

        <section>
          <h2><a href="http://playdosgamesonline.com/the-7th-guest.html">The 7th Guest</a></h2>
          <%= youtube '45Z-Q5KVTyI' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>The 7th Guest</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1993</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td><a href="https://en.wikipedia.org/wiki/Intel_80386">80386</a></td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>2MB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>Prerendered ray tracing</td>
              </tr>
            </tbody>
          </table>
          <p>
            The 7th Guest was the first PC game to appear on CD (and already that first game did not have enough with a single CD;
            it required 2 discs.) The game took place in a mansion which the player had to explore. The mansion
            was fully modelled in 3D. However, everything was prerendered. For each movement within the mansion,
            there was a separate movie clip that played showing the camera go from one location to the next.
          </p>
          <p>
            The same was true for puzzles in the game. At some point the player had to solve a puzzle involving chess pieces.
            These were rendered in 3D, but again everything was prerendered. If you moved a piece from position A to position B,
            there was a short movie clip that showed specifically that move. A different movie existed if you moved for A to C, and yet
            another for then you moved from B to C. It is clear that the game had to contain <em>many</em> such short movie clips,
            thus requiring a gigantic amount of storage, especially for that time.
          </p>
          <p>
            Since everything was prerendered, the graphics looked far better than those of any other game at the time.
          </p>
        </section>

        <section>
          <h2><a href="http://playdosgamesonline.com/ecstatica.html">Ecstatica</a></h2>
          <%= youtube '9SsR5fPjGu4' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Ecstatica</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1994</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td><a href="https://en.wikipedia.org/wiki/Intel_80486">80486</a></td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>4MB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>Ray casting + sprites</td>
              </tr>
            </tbody>
          </table>
          <p>
            Ecstatica used the same approach as Alone in the Dark, namely prerendered backgrounds with 3D characters.
            Instead of relying on 3D triangles, spheres and ellipsoids were used, resulting, somewhat unsurprisingly,
            in a less pointy look.
          </p>
        </section>

        <section>
          <h2><a href="http://playdosgamesonline.com/robinsons-requiem.html">Robinson's Requiem</a></h2>
          <%= youtube 'RllFvqrMlcY' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Robinson's Requiem</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1994</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td>?</td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>?</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>Voxels</td>
              </tr>
            </tbody>
          </table>
          <p>
            Like Comanche, Robinson's Requiem made use of voxels to render its world. This technique
            allowed for large, detailed outside worlds with few restrictions.
          </p>
        </section>
        
        <section>
          <h2><a href="http://playdosgamesonline.com/quake.html">Quake</a></h2>
          <%= youtube '21d5_Ub9YLE' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Quake</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1996</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td>Pentium 75MHz</td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>8MB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>Rasterization</td>
              </tr>
            </tbody>
          </table>
          <p>
            Quake required a Pentium 75Mhz with 8MB of memory. Contrary to Wolfenstein 3D and Doom, this game
            was truly 3D and did not rely on trickery to make 2D look like 3D. Whereas Alone in the Dark
            used triangles to model its creatures, Quake used triangles to render everything, i.e. both the game world
            and its inhabitants.
          </p>
        </section>

        <section>
          <h2><a href="https://en.wikipedia.org/wiki/Unreal">Unreal (1998)</a></h2>
          <%= youtube 'Op-tys_98Ek' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Unreal</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1998</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td>Pentium 200MHz</td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>32MB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>Optional</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>Rasterization</td>
              </tr>
            </tbody>
          </table>
          <p>
            Like Quake, Unreal fully relied on rasterization to render its graphics.
            It was one of the first games that was able to make use of a GPU.
          </p>
        </section>
        
        <section>
          <h2><a href="https://en.wikipedia.org/wiki/Outcast_(video_game)">Outcast (1999)</a></h2>
          <%= youtube 'BdB4z0xgvsw' %>
          <table class="game">
            <tbody>
              <tr>
                <td>Game</td>
                <td>Outcast</td>
              </tr>
              <tr>
                <td>Year</td>
                <td>1999</td>
              </tr>
              <tr>
                <td>CPU</td>
                <td>Pentium 200MHz</td>
              </tr>
              <tr>
                <td>RAM</td>
                <td>32MB</td>
              </tr>
              <tr>
                <td>GPU</td>
                <td>No</td>
              </tr>
              <tr>
                <td>Technique</td>
                <td>Voxels + rasterization</td>
              </tr>
            </tbody>
          </table>
          <p>
            Outcast (a Belgian game) made use of a voxel-like technology to render its landscapes but rasterization for the characters.
          </p>
        </section>

        <section>
          <h2><a href="https://en.wikipedia.org/wiki/Unreal_Engine">Unreal Engine 4 (today)</a></h2>
          <%= youtube 'HGZfUzBD8MQ' %>
          <p>
            Clearly, the triangles have won. GPUs all specialize in this technique alone, and other approaches to graphics
            are required to run on the CPU, which is of course much less efficient.
          </p>
          <p>
            20 years, 3D was faked with 2D. Today it is the other way around: even 2D games
            make use of 3D elements internally. Even <a href="https://en.wikipedia.org/wiki/Desktop_Window_Manager">OSses are using the GPU to render the desktop</a>.
          </p>
        </section>
      </section>
    </div>
  </body>
</html>
