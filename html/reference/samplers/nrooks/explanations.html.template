<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>3DCG: n-Rooks Sampler</title>
    <%= stylesheets('3dcg', 'box2', 'imgslider') %>
    <%= scripts('jquery', 'jquery-ui', 'underscore', 'ace/ace', 'source-editor', 'imgslider') %>
    <style>
      .centered {
        margin: 10px auto;
      }

      .slider {
        width: 80%;
        margin: 0px auto;
      }
    </style>

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
    </script>
    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML"></script>
  </head>

  <body>
    <header>
      n-Rooks Sampler
    </header>
    <div id="contents">
      <section>
        <h1>Definition</h1>
        <p>
          An n-rooks sampler takes a single parameter $N$. It subdivides the given rectangle in $N \times N$ subrectangles.
          It then picks $N$ random subrectangles so that in exactly one subrectangle is picked in each row and
          exactly one subrectangle is picked in each column. The name comes from the fact that if you view the subdivided
          rectangle as a chess board and rooks are placed on the selected subrectangles, that none
          of the rooks can capture any of the other rooks.
        </p>
        <p>
          From each selected subrectangle, a random point is returned.
        </p>
        <%= tex_image 'n-rooks' %>
      </section>
      <section>
        <h1>Comparison</h1>
        <section>
          <h2>n = 1</h2>
          <%= raytrace_comparison 'single', 'demo1' %>
        </section>
        <section>
          <h2>n = 2</h2>
          <%= raytrace_comparison 'single', 'demo2' %>
        </section>
        <section>
          <h2>n = 3</h2>
          <%= raytrace_comparison 'single', 'demo3' %>
        </section>
        <section>
          <h2>n = 4</h2>
          <%= raytrace_comparison 'single', 'demo4' %>
        </section>
      </section>
    </div>
  </body>

  <script>
    function initialize()
    {
      SourceEditor.initialize();
      $('.slider').slider( { showInstruction: false } );
    }

    $( initialize );   
  </script>
</html>
