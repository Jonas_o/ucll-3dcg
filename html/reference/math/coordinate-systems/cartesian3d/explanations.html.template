<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>3DCG: 3D Cartesian Coordinate System</title>
    <%= stylesheets('3dcg', 'box2') %>
    <%= scripts('jquery', 'underscore') %>
    <style>
      img.large {
        width: 60%;
      }

      table {
        margin-left: auto;
        margin-right: auto;
        width: 80%;
      }

      table {
        border-collapse; collapse;
      }

      table thead tr {
        background: #AAA;
      }

      table th {
        text-align: center;
        padding-left: 1em;
        padding-right: 1em;
      }

      table td {
        text-align: center;
      }
    </style>

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
    </script>
    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML"></script>
  </head>

  <body>
    <header>
      3D Cartesian Coordinate System
    </header>
    <div id="contents">
      <section>
        <h1>Definition</h1>
        <p>
          In a 3D Cartesian coordinate system is defined by its origin point $O$ and three perpendicular axes $X$, $Y$ and $Z$.
          The location of each point in 3D space can be expressed using coordinates $(x, y, z)$.
        </p>
        <%= tex_image '3d-cartesian' %>

        <h2>Left vs Right Handed</h2>
        <p>
          We distinguish two kinds of 3D Cartesian coordinate system: left handed and right handed ones.
          These differ in the orientation of the Z-axis with respect to the $X$ and $Y$-axis.
        </p>
        <table>
          <thead>
            <tr>
              <th>Left-handed</th>
              <th>Right-handed</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><%= tex_image 'left-handed' %></td>
              <td><%= tex_image 'right-handed' %></td>
            </tr>
          </tbody>
        </table>
        <p>
          The distinction between both becomes apparent when dealing with the <%= link 'reference/math/cross-product', 'cross product' %>.
          During this course, we will always use the right handed variant.
        </p>
      </section>
    </div>
  </body>

  <script>
    function initialize()
    {
    }

    $( initialize );    
  </script>

</html>
