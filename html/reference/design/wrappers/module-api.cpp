namespace raytracer
{
  namespace primitives
  {
    class Primitive { ... };

    Primitive sphere();
    Primitive cube();
    Primitive cone();
    ...
  }
}

// Creating a sphere
Primitive sphere = raytracer::primitives::sphere();

