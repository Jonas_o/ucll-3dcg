#include "primitives/primitive.h"
#include "primitives/plane-primitive.h"
#include "primitives/sphere-primitive.h"
#include "primitives/cylinder-primitive.h"
#include "primitives/disk-primitive.h"
#include "primitives/square-primitive.h"
#include "primitives/cube-primitive.h"
#include "primitives/cropper-primitive.h"
#include "primitives/cone-primitive.h"
#include "primitives/group-primitive.h"
#include "primitives/union-primitive.h"
#include "primitives/decorator-primitive.h"
#include "primitives/intersection-primitive.h"
#include "primitives/difference-primitive.h"
#include "primitives/transformer-primitive.h"
#include "primitives/triangle-primitive.h"
#include "primitives/bounding-box-accelerator-primitive.h"
#include "primitives/mesh-primitive.h"
#include "primitives/fast-mesh-primitive.h"
#include "primitives/bumpifier-primitive.h"
