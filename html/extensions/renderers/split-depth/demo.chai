def scene_at(now)
{
  var camera = Cameras.perspective( [ "eye": pos(0, 0, 5),
                                      "look_at": pos(0, 0, 0),
                                      "up": vec(0, 1, 0),
                                      "distance": 1,
                                      "aspect_ratio": 1 ] )
                                      
  var white = Materials.uniform( [ "ambient": Colors.white() * 0.1,
                                   "diffuse": Colors.white() * 0.8,
                                   "specular": Colors.white(),
                                   "specular_exponent": 20,
                                   "reflectivity": 0,
                                   "transparency": 0,
                                   "refractive_index": 0 ] )
                                   
  var cyl = scale(0.25, 0.25, 1, cylinder_along_z())
  var t = Animations.animate(0, 1, seconds(1))

  var angle = degrees(sin( degrees(360 * t[now]) ) * 30)

  var root   = decorate(white, union( [ rotate_around_y(angle, translate(vec(0,-1,0), cyl)),
                                        rotate_around_y(-angle, translate(vec(0,1,0),cyl)) ] ))
  var lights = [ Lights.omnidirectional( pos(0, 0, 5), Colors.white() ) ]
  
  return create_scene(camera, root, lights)
}

var raytracer = Raytracers.v6()

var renderer = Renderers.split_depth( [ "width": 500,
                                        "height": 500,
                                        "sampler": Samplers.multijittered(2),
                                        "ray_tracer": raytracer,
                                        "split_thickness": 0.1,
                                        "eye": pos(0, 1, 5),
                                        "look_at": pos(0, 0, 0) ] )

pipeline( scene_animation(scene_at, seconds(1)),
          [ Pipeline.animation(30),
            Pipeline.renderer(renderer),
            Pipeline.ppm() ] )
