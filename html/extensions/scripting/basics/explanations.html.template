<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>3DCG: Scripting Basics</title>
    <%= stylesheets('3dcg', 'box2') %>
    <%= scripts('jquery', 'jquery-ui', 'underscore', 'ace/ace', 'source-editor') %>
    <style>
      img.large {
        width: 60%;
      }

      video {
        margin: 10px auto;
      }
    </style>

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
    </script>
    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML"></script>
  </head>

  <body>
    <header>
      Scripting Basics
    </header>
    <div id="contents">
      <section>
        <h1>Rendering a Script</h1>
        <p>
          The ray tracer comes with a built-in scripting language. This makes it possible
          to create new scenes or modify existing ones without having to recompile each time.
          The language (<a href="http://chaiscript.com/">ChaiScript</a>) also ought to be simpler
          to use than C++.
        </p>
        <p>
          In order to be able to render a script, we need to make some changes to <code>app.cpp</code> first,
          which is where the <code>main</code> function resides.
          By default, the ray tracer will render the <code>basic_sample</code> and write it away to file.
          Instead, you want it to process the command line arguments, so that when on the console, launching
          the ray tracer using raytracer <code>-s<span style="font-style: italic;">script</span></code>
          lets it read in <span style="font-style: italic;">script</span> and execute all its instructions.
        </p>
        <aside class="boxed2">
          <h1>Task</h1>
          <p>
            Perform the following steps:
          </p>
          <ul>
            <li>
              Modify <code>app.cpp</code> such that <code>main</code> calls <code>process_command_line_argument</code>
              instead of running <code>basic_sample</code>. Recompile in release build.
            </li>
            <li>
              Locate the produced executable. It should be under <code>raytracer/x64/Release</code>.
            </li>
            <li>
              Create a file <code>test.chai</code> in the executable's directory.
              Give it the following contents:
              <%= source 'test.chai' %>
              Examine the script carefully. You should be able to understand what each line means.
            </li>
            <li>
              Open a terminal in the executable's directory.
            </li>
            <li>
              Execute <code>raytracer -stest.chai</code> and wait for the ray tracer to finish.
            </li>
            <li>
              Open the WIF file.
            </li>
          </ul>
        </aside>
        <p>
          If all goes well, you should the the following animation:
        </p>
        <%= raytrace_movie 'test-ppm' %>
        <p>
          Is this what you expected to see? Make sure you understand why this is the correct animation.
        </p>
        <aside class="boxed2">
          <h1>Task</h1>
          <p>
            Write a script that produces the following animation:
          </p>
          <%= raytrace_movie 'exercise' %>
          <p>
            Some help may be required:
          </p>
          <ul>
            <li>
              You obviously need two spheres. Unions are created as follows: <code>union( [ a, b, c ] )</code>
              where <code>a</code>, <code>b</code> and <code>c</code> are the union's elements.
            </li>
            <li>
              Translation is done using <code>translate(vec(x, y, z), primitive)</code>.
            </li>
            <li>
              To normalize a vector, you can invoke its <code>normalized()</code> function.
              For example: <code>vec(1, 2, 3).normalized()</code>.
            </li>
          </ul>
          <p>
            <span style="font-weight: bold;">Hint</span> Even though the spheres appear to rotate, they are immobile. Rotation is not implemented yet anyway,
            and while you could implement rotation in the scripting language, no such advanced trickery is required.
          </p>
        </aside>
      </section>      
    </div>
  </body>

  <script>
    function initialize()
    {
      SourceEditor.initialize();
    }

    $( initialize );   
  </script>
</html>
