<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>3DCG: Ray Tracer v1</title>
    <%= stylesheets('3dcg', 'box2') %>
    <%= scripts('jquery', 'jquery-ui', 'underscore', 'ace/ace', 'source-editor') %>
    <style>
      img.large {
        width: 60%;
      }

      video {
        margin: 10px auto;
      }
    </style>

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
    </script>
    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML"></script>
  </head>

  <body>
    <header>
      Ray Tracer v1
    </header>
    <div id="contents">
      <%= overview(reading_material: ['lighting/ambient']) %>
      
      <section>
        <h1>Preview</h1>
        <%= raytrace_movie 'demo' %>
        <%= source 'demo.chai' %>
      </section>

      <section>
        <h1>Introducing ray tracer v1</h1>
        <p>
          Go take a look at the code for original ray tracer in <code>raytracers/ray-tracer-v0</code>.
          The <code>trace</code> member function must, given a ray and a scene, determine
          what (among other things) determine which object in the scene is hit by the ray first
          and find out what color this object has.
        </p>
        <p>
          Ray tracer v0 is a rather lazy implementation: if there's a hit, it simply returns white instead
          of actually trying to find out which color the object has. Ray tracer v1 will improve upon.
        </p>
        <aside class="boxed2">
          <h1>Task</h1>
          <p>
            Add the following files to the project:
          </p>
          <ul>
            <li>
              <code>raytracers/ray-tracer-v1.cpp</code>
            </li>
            <li>
            <code>raytracers/ray-tracer-v1.h</code>
            </li>
          </ul>
          <p>
            Copy the contents of <code>raytracers/ray-tracer-v0.cpp</code> and <code>raytracers/ray-tracer-v0.h</code> into them.
            Update the code for the name change, e.g.
          </p>
          <ul>
            <li>
              In <code>ray-tracer-v1.h</code>, change the class name from <code>RayTracerV0</code> to <code>RayTracerV1</code>.
            </li>
            <li>
              In <code>ray-tracer-v1.h</code>, change the factory function's name from <code>v0</code> to <code>v1</code>.
            </li>
            <li>
              In <code>ray-tracer-v1.cpp</code>, change <code>#include "raytracers/ray-tracer-v0.h"</code> to <code>#include "raytracers/ray-tracer-v1.h"</code>.
            </li>
            <li>
              And so on.
            </li>
          </ul>
          <p>
            To check your work,
          </p>
          <ul>
            <li>
              Create a new demo file by copying <code>basic-sample</code> and updating it so as to use ray tracer <code>v1</code> instead of <code>v0</code>. You
              may have to take a look into the superclass <code>Demo</code> to get this done.
            </li>
            <li>
              Don't forget to update <code>app.cpp</code> so that it makes use of the new demo.
            </li>
            <li>
              Compile.
            </li>
            <li>
              Run. It should produce exactly the same results as before your change.
            </li>
          </ul>
        </aside>
      </section>
      <section>
        <h1>Ambient Lighting</h1>
        <p>
          Ray tracer v1 adds support for <%= link 'reference/lighting/ambient', 'ambient lighting' %>. Be sure to read the linked material
          as otherwise you will not understand what you need to implement.
        </p>
        <p>
          There are multiple ways to introduce ambient lighting to our RayTracer. We chose to associate a ambient lighting
          factor to materials: you can create a sphere with a lot of ambient lighting and just to it another
          sphere with no ambient lighting. This is highly unrealistic, but it's up to the creator of the scene to avoid such pitfalls.
        </p>
        <aside class="boxed2">
          <h1>Task</h1>
          <p>
            In order to add support for ambient lighting, you need to update <code>RayTracerV1::trace</code> as follows:
          </p>
          <ul>
            <li>
              The function should start with finding the first positive hit between the given ray and the given scene.
            </li>
            <li>
              If a hit is found, the <code>hit</code> object will contain all necessary information you need to continue.
              Most noteworthy is the <code>hit</code> object's <code>material</code> field, which contains information
              about the material that's been hit by the ray. For example, if the ray hits a tree, the <code>material</code> field
              will contain information about wood.
            </li>
            <li>
              Materials are not necessarily uniform. For example, wood is seldom homogeneously brown but contains lines and rings.
              This means there is not one single material color. Instead of asking the material "What is your color?",
              you need to ask "What is your color <em>at that location</em>?" This is done using the <code>at</code> method:
              it takes a <code>Point3D</code> and returns a <code>MaterialProperties</code> object that contains location-specific information.
            </li>
            <li>
              The <code>Point3D</code> you need to pass to <code>Material::at</code> is the primitive's <em>local</em> hit position.
              You can find it in the <code>hit</code> object (it was initialized by <code>find_first_positive_hit</code>).
            </li>
            <li>
              <code>trace</code> must return a <code>TraceResult</code>, which has four components. The <code>group_id</code>,
              <code>ray</code> and <code>hit</code> parts can be initialized in the same way is in <code>RayTracerV0</code>.
              The color component is different though: instead of always using white, you should return the color returned by the material.
              You can find it in the <code>MaterialProperties</code> object.
            </li>
          </ul>
        </aside>
        <p>
          It is of paramount importance that you <em>always</em> check that your changes work.
        </p>
        <aside class="boxed2">
          <h1>Task</h1>
          <p>
            Run your project. Everything should look black since instead of returning white you return the material's ambient color, which is black.
          </p>
          <p>
            Now modify <code>basic-sample.cpp</code> so that the sphere has a red as ambient color (use <code>colors::red()</code>).
            Compile and run to see if it works.
          </p>
        </asidesection>
      </section>
      <section>
        <h1>Scripting</h1>
        <p>
          We want to make our new ray tracer available for scripting. For each extension we make, we need to "export" it
          to the scripting language.
        </p>
        <aside class="boxed2">
          <h1>Task</h1>
          <p>
            Open the file <code>scripting/raytracing-module.cpp</code>.
          </p>
        </aside>
        <p>
          Right now, the scripting language knows only about one ray tracer, namely v0. You can create it within the scripting
          language as follows: <code>var raytracer = Raytracers.v0()</code>. Our goal is to make <code>Raytracers.v1()</code>
          create a v1 ray tracer.
        </p>
        <p>
          <code>v0</code> and <code>v1</code> are in essence ray tracer factories. We grouped all ray traces factories
          into some kind of package named <code>Raytracers</code>. The same thing applies for other kinds of factories,
          for example, all samplers can be created using <code>Samplers.<span style="font-style: italic;">name</span>()</code>,
          such as <code>Samplers.single()</code> or <code>Samplers.nrooks(3)</code>.
        </p>
        <p>
          In order to have these factories grouped in a package, we need to create C++ object
          that contains all these factories. If you go take a look in <code>scripting/raytracing-module</code>, you'll
          see that there is a struct called <code>RaytracerLibrary</code> which contains a member function <code>v0()</code>.
          This member function has the sole responsibility of creating a <code>v0</code> ray tracer object.
        </p>
        <p>
          To inform the scripting language of this library object, we undertake the following steps (see
          <code>raytracer::scripting::_private_::create_raytracing_module</code> in <code>scripting/raytracing-module</code>):
        </p>
        <ol>
          <li>
            We create the library object.
          </li>
          <li>
            We declare a constant <code>Raytracers</code> within the scripting language which
            is then linked with the object. This is done using <code>module-&gt;add_global_const</code>.
          </li>
          <li>
            Next, we must expose each member function in turn. This is achieved
            with <code>module-&gt;add</code>. Since the syntax is not trivial
            and this needs to be done for each ray tracer version,
            we defined a macro <code>BIND</code> that takes most of the syntactic burden
            away from us: if <code>RaytracerLibrary</code> has a member function F,
            you can expose it using <code>BIND(F)</code>.
          </li>
        </ol>
        <p>
          We do not put all factories within packages, as it would become too much of a syntactic
          burden for often used factories. For example, primitives can be created
          using top level functions, i.e. you simply write <code>sphere()</code> instead
          of <code>Primitives.sphere()</code>.
        </p>
        <p>
          An example of how to export something as a top level function is <code>create_scene</code>.
          In C++, <code>create_scene</code> is a top level function. <code>module-&gt;add</code> recognizes
          this fact and mirrors it by exposing <code>create_scene</code> as a top level function inside the scripting language.
        </p>
        <aside class="boxed2">
          <h1>Task</h1>
          <p>
            Add a binding for <code>v1</code>.
          </p>
          <p>
            Examine the <code>v(int)</code> function and update it as necessary.
          </p>
          <p>
            Check your work by creating a script that uses ray tracer v1 and rendering it.
          </p>
        </aside>
      </section>
    </div>
  </body>

  <script>
    function initialize()
    {
      SourceEditor.initialize();
    }

    $( initialize );   
  </script>
</html>
