\chapter{Complex Numbers and Quaternions}
\begin{Definition}[imaginary unit] \index{imaginary number}\index{$i$}
The \emph{imaginary unit} $i$ is a value satisfying $i^2 = -1$.
\end{Definition}

\begin{Definition}[complex number] \index{complex number}
A \emph{complex number} is a number of the form $a + b i$ with $a, b \in \real$.
\end{Definition}

\begin{Definition}[quaternion] \index{conjugate}
A \emph{quaternion} is a number of the form $a + bi + cj + dk$ with $a,b,c,d \in \real$ and $i,j,k$ satisfying the following properties
\[
  \begin{array}{c|ccc}
    \times & i & j & k \\
    \hline
    i & -1 & k & -j \\
    j & -k & -1 & i \\
    k & j & -i & -1
  \end{array}
\]
Note that these multiplications are \emph{not} commutative.
\end{Definition}


\begin{Definition}[conjugate] \index{conjugate}\index{quaternion!conjugate}
The \emph{conjugate} of a quaternion $q = a+bi+cj+dk$ is defined as
\[
  \conj{q} = a -bi-cj-dk
\]
\end{Definition}

\begin{Definition}[rotation quaternion] \index{rotation quaternion}\index{quaternion!rotation}
The \emph{rotation quaternion} for a rotation of angle $\theta$ around
an axis represented by the unit vector $(r_x, r_y, r_z)$ is defined as
\[
  q = \cos\left(\frac\theta2\right) +
      \sin\left(\frac\theta2\right) r_x i + 
      \sin\left(\frac\theta2\right) r_y j + 
      \sin\left(\frac\theta2\right) r_z k
\]
\end{Definition}

\begin{theorem} \index{rotation}
In order to apply a rotation represented by a quaternion $q$ on a point $P(x,y,z)$, perform
the following steps:
\begin{enumerate}
  \item Let $p = xi+yj+zk$.
  \item Let $p' = q \cdot p \cdot \conj{q}$ which is of the form $x'i+y'j+z'k$.
  \item The result of the rotation is $(x',y',z')$.
\end{enumerate}
\end{theorem}

\begin{extra}
  \begin{example}
  Rotating $P(2,0,0)$ $180\degrees$ around the axis $(0,1,0)$ should yield $(-2,0,0)$.
  The rotation quaternion equals
  \[
    q = \cos\left(90\degrees\right) +
        \sin\left(90\degrees\right) \cdot 0 \cdot i + 
        \sin\left(90\degrees\right) \cdot 1 \cdot j + 
        \sin\left(90\degrees\right) \cdot 0 \cdot k = j
  \]
  We have $p = 2i+0j+0k = 2i$. We perform quaternion multiplication:
  \[
    q \cdot p \cdot \conj{q} = j \cdot 2i \cdot (-j) = -2 j i j = -2 (-k) j =  -2i
  \]
  which corresponds to the point with coordinates $(-2,0,0)$.
  \end{example}
\end{extra}

\begin{extra}
  \begin{example}
    Quaternions represent rotations around an axis that goes through the origin $(0,0,0)$.
    What if we want to rotate around an axis that does not go through the origin?
    Say we want to rotate $P$ around an axis parallel to the Z-axis, as shown below.
    \begin{center}
      \begin{tikzpicture}
        \draw[axis] (0,-1) -- (0,4) node[at end,right] {Y};
        \draw[axis] (-1,0) -- (4,0) node[at end,above] {X};
        \point[position={(3,2)}]
        \point[position={(4,2)},label={P},anchor=west]
        \point[position={(3,3)},label={P'},anchor=south]
        \draw[-latex,thick] (4,2) arc [start angle=0,end angle=90,radius=1cm];
      \end{tikzpicture}
    \end{center}

    The achieve this, we first translate everything so that the rotation axis
    does go through (0,0,0), we perform the rotation and lastly translate everything back.

    \begin{center}
      \begin{tikzpicture}
        \draw[axis] (0,-1) -- (0,4) node[at end,right] {Y};
        \draw[axis] (-1,0) -- (4,0) node[at end,above] {X};
        \point[position={(3,2)}]
        \point[position={(4,2)},label={P},anchor=west]
        \point[position={(3,3)},label={P'},anchor=south]
        \draw[-latex,thick] (1,0) arc [start angle=0,end angle=90,radius=1cm] node[midway,font=\tiny,anchor=south west] {step 2};

        \draw[dashed,-latex] (4,2) -- (1,0) node[midway,below,sloped,font=\tiny] {step 1};
        \draw[dashed,-latex] (0,1) -- (3,3) node[midway,above,sloped,font=\tiny] {step 3};
      \end{tikzpicture}
    \end{center}

    It is certainly possible to come up with a formula that can directly deal
    with rotations around arbitrary axes (i.e.\ axes not going through the origin),
    but it would be so unwieldy that it is much simpler to work with a series of
    basic transformations (translation + rotation + translation).
  \end{example}
\end{extra}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "reference"
%%% End:
